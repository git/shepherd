# GNU Shepherd --- Test logging behavior when failing to write log file.
# Copyright © 2025 Ludovic Courtès <ludo@gnu.org>
#
# This file is part of the GNU Shepherd.
#
# The GNU Shepherd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# The GNU Shepherd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the GNU Shepherd.  If not, see <http://www.gnu.org/licenses/>.

shepherd --version
herd --version

log_directory="$PWD/t-log-directory-$$"
socket="t-socket-$$"
conf="t-conf-$$"
log="t-log-$$"
pid="t-pid-$$"

herd="herd -s $socket"

trap "cat $log || true; rm -f $log $socket $conf $pid; rm -rf $log_directory;
      kill \$main_pid || true" EXIT

cat > "$conf"<<EOF
(register-services
  (list (service
          '(log-directory-does-not-exist)
          #:start (make-forkexec-constructor
                   '("$SHELL" "-c" "while true; do echo logging; sleep 0.2; done")
                   #:log-file "$log_directory/service.log")
          #:stop (make-kill-destructor))
        (service
          '(log-directory-not-writable)
          #:start (make-forkexec-constructor
                   '("$SHELL" "-c" "while true; do echo logging; sleep 0.2; done")
                   #:log-file "/proc/1/seriously?.log")
          #:stop (make-kill-destructor))))
EOF

rm -f "$pid"

shepherd -I -s "$socket" -c "$conf" -l "$log" --pid="$pid" &
main_pid=$!

# Wait till it's ready.
until test -f "$pid" ; do sleep 0.3 ; done

$herd status

# "$log_directory" does not and should be automatically created.
test -f "$log_directory" && false
$herd start log-directory-does-not-exist
$herd status log-directory-does-not-exist | grep running
$herd status log-directory-does-not-exist | grep "$log_directory/service.log"
grep logging "$log_directory/service.log"

# If the log file cannot be created at all, the service fails to start.
$herd start log-directory-not-writable && false
$herd status log-directory-not-writable | grep stopped
grep "Failed to open log file .* for log-directory-not-writable" "$log"

$herd status
